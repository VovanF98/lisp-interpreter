#include <stdexcept>

#ifndef LISPP_LISPNAMEERROR_H
#define LISPP_LISPNAMEERROR_H


class LispNameException: public std::logic_error {
public:
    LispNameException(const std::string& msg)
            : logic_error(msg){};
};


#endif //LISPP_LISPNAMEERROR_H
