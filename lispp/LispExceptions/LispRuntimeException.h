#include <stdexcept>

#ifndef LISPP_LISPRUNTIMEEXCEPTION_H
#define LISPP_LISPRUNTIMEEXCEPTION_H


class LispRuntimeException: public std::logic_error {
public:
    LispRuntimeException(const std::string& msg)
            : logic_error(msg){};
};


#endif //LISPP_LISPRUNTIMEEXCEPTION_H
