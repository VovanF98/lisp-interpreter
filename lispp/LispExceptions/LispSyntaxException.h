#include <stdexcept>

#ifndef LISPP_LISPSYNTAXEXCEPTION_H
#define LISPP_LISPSYNTAXEXCEPTION_H


class LispSyntaxException: public std::logic_error {
public:

    LispSyntaxException(const std::string& msg)
            : logic_error(msg){};
};


#endif //LISPP_LISPSYNTAXEXCEPTION_H
