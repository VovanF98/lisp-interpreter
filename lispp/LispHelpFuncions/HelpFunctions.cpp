#include "HelpFunctions.h"
#include <stack>
#include "../LispTypes/LambdaType.h"
#include <iostream>

bool isFunctionType(std::shared_ptr<Type> ptr) {
    return std::dynamic_pointer_cast<Function>(ptr) != nullptr;
}

bool isPairType(std::shared_ptr<Type> ptr) {
    return std::dynamic_pointer_cast<PairType>(ptr) != nullptr;
}

bool isIntegerType(std::shared_ptr<Type> ptr) {
    return std::dynamic_pointer_cast<IntegerType>(ptr) != nullptr;
}

bool isSymbolType(std::shared_ptr<Type> ptr) {
    return std::dynamic_pointer_cast<SymbolType>(ptr) != nullptr;
}

std::vector<std::shared_ptr<Type>> toVectorEval(std::shared_ptr<PairType> pair, std::shared_ptr<Scope> &scope) {

    std::vector<std::shared_ptr<Type>> args;

    if(pair == nullptr) {
        return args;
    }

    auto cur_pair = pair;

    while(cur_pair.get()->GetNext()) {

        args.push_back(cur_pair.get()->GetCur().get()->eval(scope));

        if(isPairType(cur_pair.get()->GetNext()))
            cur_pair = std::dynamic_pointer_cast<PairType>(cur_pair.get()->GetNext());
        else {
            args.push_back(cur_pair.get()->GetNext());
            break;
        }
    }

    if(cur_pair.get()->GetCur().get())
        args.push_back(cur_pair.get()->GetCur().get()->eval(scope));

    return args;
}

std::vector<std::shared_ptr<Type>> toVectorNoEval(std::shared_ptr<PairType> pair) {

    std::vector<std::shared_ptr<Type>> args;

    if(pair == nullptr) {
        return args;
    }

    auto cur_pair = pair;

    while(cur_pair.get()->GetNext()) {

        args.push_back(cur_pair->GetCur());

        cur_pair = std::dynamic_pointer_cast<PairType>(cur_pair.get()->GetNext());
    }

    return args;
}

std::shared_ptr<Type> isList(std::string& ptr) {

    for(int i = 0; i < ptr.size(); ++i) {
        if (ptr[i] == '.') {
            return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
        }
    }
    return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
}

bool isLambdaType(std::shared_ptr<Type> ptr) {
    return std::dynamic_pointer_cast<LambdaType>(ptr) != nullptr;
}
