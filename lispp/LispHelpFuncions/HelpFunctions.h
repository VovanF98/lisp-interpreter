#pragma once

#include <deque>
#include "../AllTypes.h"

bool isFunctionType(std::shared_ptr<Type> ptr);

bool isPairType(std::shared_ptr<Type> ptr);

bool isIntegerType(std::shared_ptr<Type> ptr);

bool isSymbolType(std::shared_ptr<Type> ptr);

std::vector<std::shared_ptr<Type>> toVectorEval(std::shared_ptr<PairType> pair, std::shared_ptr<Scope> &scope);

std::vector<std::shared_ptr<Type>> toVectorNoEval(std::shared_ptr<PairType> pair);

std::shared_ptr<Type> isList(std::string& ptr);

bool isLambdaType(std::shared_ptr<Type> ptr);
