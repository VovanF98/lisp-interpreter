#include "Parser.h"
#include <cstring>
#include "../LispExceptions/LispSyntaxException.h"

std::shared_ptr<Type> Parser::BuildElement(std::string& token)
{
    if (isdigit(token[0]) || (token[0] == '-' && isdigit(token[1])) || (token[0] == '+' && isdigit(token[1])))
    {
        auto num = static_cast<int64_t>(std::stoi(token));
        return std::shared_ptr<Type>(new IntegerType(num));
    }
    else {
        return std::shared_ptr<Type>(new SymbolType(token));
    }
}

std::shared_ptr<Type> Parser::BuildAST(std::deque<std::string>& tokens) {
    std::string token(tokens.front());
    tokens.pop_front();
    if (token == "(") {
        auto CurrentPair = std::make_shared<PairType>();
        auto cur_pair = CurrentPair;
        while(tokens.front() != ")") {
            auto NextPair = std::static_pointer_cast<Type>(std::make_shared<PairType>());
            auto current = BuildAST(tokens);
            if (tokens.empty()) {
                throw LispSyntaxException("Unexpected end of expression.");
            }
            cur_pair.get()->SetCur(current);
            cur_pair.get()->SetNext(NextPair);
            if(tokens.front() == ".") {
                tokens.pop_front();
                auto el = BuildAST(tokens);
                cur_pair.get()->SetNext(el);
                break;
            }
            cur_pair = std::dynamic_pointer_cast<PairType>(cur_pair.get()->GetNext());
        }
        if (tokens.empty())
            throw LispSyntaxException("Unexpected end of expression.");
        tokens.pop_front();
        return CurrentPair;
    }
    if (token == "'") {
        token = "Quote";
        return std::static_pointer_cast<Type>(std::make_shared<PairType>(BuildElement(token), BuildAST(tokens)));
    }
    return BuildElement(token);
}

std::deque<std::string> Parser::Tokenize(const std::string & str)
{
    std::deque<std::string> tokens;
    const char * s = str.c_str();
    while (*s) {
        while (*s == ' ')
            ++s;
        if(*s == '\'') {
            tokens.push_back("'");
            ++s;
            continue;
        }
        if (*s == '(' || *s == ')')
            tokens.push_back(*s++ == '(' ? "(" : ")");
        else {
            const char * t = s;
            while (*t && *t != ' ' && *t != '(' && *t != ')')
                ++t;
            tokens.push_back(std::string(s, t));
            s = t;
        }
    }
    return tokens;
}

std::shared_ptr<Type> Parser::Parse(std::string &s) {
    std::deque<std::string> tokens(Tokenize(s));
    auto AST = BuildAST(tokens);
    return AST;
}
