#include <string>
#include "../LispTypes/Type.h"
#include "../Scope.h"

class Interpreter {
public:
    ~Interpreter();
    Interpreter();
    std::string run(std::string& input);
    void repl();
    std::string toString(std::shared_ptr<Type>& ptr);
private:
    std::shared_ptr<Scope> scope;
};
