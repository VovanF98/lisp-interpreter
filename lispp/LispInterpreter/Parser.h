#include <deque>
#include <string>
#include "../AllTypes.h"

class Parser {
public:
    std::deque<std::string> Tokenize(const std::string& to_tokenize);
    std::shared_ptr<Type> BuildElement(std::string& token);
    std::shared_ptr<Type> BuildAST(std::deque<std::string>& tokens);
    std::shared_ptr<Type> Parse(std::string & s);
};

