#include "Interpreter.h"
#include "Parser.h"
#include "../LispHelpFuncions/HelpFunctions.h"
#include "../LispExceptions/LispRuntimeException.h"

#include <iostream>

Interpreter::Interpreter() {
    scope = std::make_shared<Scope>();
}

Interpreter::~Interpreter() {
    (*scope.get()).clearScope();
}

void Interpreter::repl() {

    std::string input;
    Parser parser;
    auto scope = std::make_shared<Scope>();

    while(true){

        std::getline(std::cin, input);
        if (input == "Quit") {
            break;
        }

        auto AST = std::dynamic_pointer_cast<PairType>(parser.Parse(input));
        auto expression = AST.get()->eval(scope);
        auto answer = toString(expression);
        std::cout << answer;
    }
}

std::string Interpreter::toString(std::shared_ptr<Type>& ptr) {
    return ptr.get()->ToString();
}

std::string Interpreter::run(std::string &input) {
    Parser parser;
    auto FirstPair = parser.Parse(input);
    if(isIntegerType(FirstPair)) {
        return FirstPair.get()->ToString();
    }
    if(isSymbolType(FirstPair)) {
        auto name = FirstPair.get()->ToString();
        if(!scope.get()->isInScope(name))
            throw LispRuntimeException("There is no variable/function/lambda with such name declared.");
        return FirstPair.get()->eval(scope).get()->ToString();
    }
    auto AST = std::dynamic_pointer_cast<PairType>(parser.Parse(input));

    return AST.get()->eval(scope)->ToString();
}
