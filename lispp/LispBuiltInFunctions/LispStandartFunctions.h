#ifndef LISPP_LISPSTANDARTFUNCTIONS_H
#define LISPP_LISPSTANDARTFUNCTIONS_H
#include "../LispTypes/Type.h"

std::shared_ptr<Type> proc_add(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_sub(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_mul(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_div(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_equal(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_greater(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_less(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_less_equal(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_greater_equal(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_max(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_min(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_abs(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_cons(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_car(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_cdr(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_set_car(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_set_cdr(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_list(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_list_ref(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_list_tail(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_not(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_and(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_or(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_is_eq(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_is_nil(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_is_pair(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_is_int(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_is_bool(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_list_ref(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_list_tail(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_is_symbol(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_is_list(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_is_equal(std::vector<std::shared_ptr<Type>>& args);

std::shared_ptr<Type> proc_is_int_equal(std::vector<std::shared_ptr<Type>>& args);

#endif //LISPP_LISPSTANDARTFUNCTIONS_H
