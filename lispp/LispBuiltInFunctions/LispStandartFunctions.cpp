#include "LispStandartFunctions.h"
#include "../LispHelpFuncions/HelpFunctions.h"
#include <iostream>
#include <algorithm>
#include "../LispExceptions/LispRuntimeException.h"


std::shared_ptr<Type> proc_add(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    for (int i = 0; i < args_size; ++i) {
        auto z = args[i].get()->ToString();
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"+\" function.");
        }
    }

    int64_t sum = 0;

    for (int i = 0; i < args_size; ++i) {
        sum += stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString());
    }

    auto ans = std::make_shared<IntegerType>(sum);
    return std::static_pointer_cast<Type>(ans);
}

std::shared_ptr<Type> proc_sub(std::vector<std::shared_ptr<Type>>& args) {

    if (static_cast<int64_t>(args.size()) == 1 || args.empty()) {
        throw LispRuntimeException("Too few arguments passed to \"-\" function.");
    }

    for (int i = 0; i < static_cast<int64_t>(args.size()); ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"-\" function.");
        }
    }

    int64_t sum = stoi(std::dynamic_pointer_cast<IntegerType>(args[0]).get()->ToString());
    for (int i = 1; i < static_cast<int64_t>(args.size()); ++i) {
        sum -= stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString());
    }

    auto ans = std::make_shared<IntegerType>(sum);
    return std::static_pointer_cast<Type>(ans);
}

std::shared_ptr<Type> proc_mul(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"*\" function.");
        }
    }

    int64_t sum = 1;
    for (int i = 0; i < args_size; ++i) {
        sum *= stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString());
    }

    auto ans = std::make_shared<IntegerType>(sum);
    return std::static_pointer_cast<Type>(ans);
}

std::shared_ptr<Type> proc_div(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    if (args_size == 1 || args_size == 0) {
        throw LispRuntimeException("Too few arguments passed to \"/\" function.");
    }

    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"/\" function.");
        }
    }

    int64_t sum = stoi(std::dynamic_pointer_cast<IntegerType>(args[0]).get()->ToString());

    for (int i = 1; i < args_size; ++i) {
        if (std::dynamic_pointer_cast<IntegerType>(args[i])) {
            sum /= stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString());
        }
    }

    auto ans = std::make_shared<IntegerType>(sum);
    return std::static_pointer_cast<Type>(ans);
}

std::shared_ptr<Type> proc_less(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();

    if (args_size == 1) {
        throw LispRuntimeException("Too few arguments passed to \"<\" function.");
    }

    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"<\" function.");
        }
    }

    int64_t pivot = 0;
    if (args_size > 0) {
        pivot = stoi(std::dynamic_pointer_cast<IntegerType>(args[0]).get()->ToString());
    }

    bool ans = true;

    for (int i = 1; i < static_cast<int64_t>(args.size()); ++i) {
        if (std::stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString()) <= pivot) {
            ans = false;
        }
    }

    if (ans) {
        return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
    }

    return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
}

std::shared_ptr<Type> proc_less_equal(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    if (args_size == 1) {
        throw LispRuntimeException("Too few arguments passed to \"<=\" function.");
    }

    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"<=\" function.");
        }
    }

    int64_t pivot = 0;
    if (args_size > 0) {
        pivot = stoi(std::dynamic_pointer_cast<IntegerType>(args[0]).get()->ToString());
    }

    bool ans = true;

    for (int i = 1; i < args_size; ++i) {
        if (std::stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString()) < pivot) {
            ans = false;
        }
    }

    if (ans) {
        return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
    }

    return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
}

std::shared_ptr<Type> proc_greater(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();

    if (args_size == 1) {
        throw LispRuntimeException("Too few arguments passed to \">\" function.");
    }

    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \">\" function.");
        }
    }

    int64_t pivot = 0;
    if (args_size > 0) {
        pivot = stoi(std::dynamic_pointer_cast<IntegerType>(args[0]).get()->ToString());
    }

    bool ans = true;

    for (int i = 1; i < args_size; ++i) {
        if (std::stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString()) >= pivot) {
            ans = false;
        }
    }

    if (ans) {
        return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
    }

    return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
}

std::shared_ptr<Type> proc_greater_equal(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    if (args_size == 1) {
        throw LispRuntimeException("Too few arguments passed to \">=\" function.");
    }

    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \">=\" function.");
        }
    }

    int64_t pivot = 0;
    if (args_size > 0) {
        pivot = stoi(std::dynamic_pointer_cast<IntegerType>(args[0]).get()->ToString());
    }

    bool ans = true;

    for (int i = 1; i < args_size; ++i) {
        if (std::stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString()) > pivot) {
            ans = false;
        }
    }

    if (ans) {
        return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
    }

    return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
}

std::shared_ptr<Type> proc_is_int(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    if (args_size > 1) {
        throw LispRuntimeException("Too many arguments passed to \"int?\" function.");
    }

    bool is_int = isIntegerType(args[0]);

    if (is_int) {
        return std::make_shared<TrueType>();
    }

    return std::make_shared<FalseType>();
}

std::shared_ptr<Type> proc_equal(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    if (args_size == 1) {
        throw LispRuntimeException("Too few arguments passed to \"=\" function.");
    }

    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"=\" function.");
        }
    }

    bool ans = true;
    int64_t pivot = 0;

    if (args_size > 0) {
        pivot = stoi(std::dynamic_pointer_cast<IntegerType>(args[0]).get()->ToString());
    }

    for (int i = 1; i < args_size; ++i) {
        if (std::stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString()) != pivot) {
            ans = false;
        }
    }

    if(ans) {
        return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
    }

    return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
}

std::shared_ptr<Type> proc_max(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    if (args_size == 0) {
        throw LispRuntimeException("Too few arguments passed to \"max\" function.");
    }

    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"max\" function.");
        }
    }

    int64_t pivot = std::stoi(std::dynamic_pointer_cast<IntegerType>(args[0]).get()->ToString());

    for (int i = 1; i < args_size; ++i) {
        pivot = std::max(pivot, (int64_t)(std::stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString())));
    }

    return std::make_shared<IntegerType>(pivot);
}

std::shared_ptr<Type> proc_min(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    if (args_size == 0) {
        throw LispRuntimeException("Too few arguments passed to \"min\" function.");
    }

    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"min\" function.");
        }
    }

    int64_t pivot = std::stoi(std::dynamic_pointer_cast<IntegerType>(args[0]).get()->ToString());

    for (int i = 1; i < args_size; ++i) {
        pivot = std::min(pivot,
                         (int64_t)(std::stoi(std::dynamic_pointer_cast<IntegerType>(args[i]).get()->ToString())));
    }

    return std::make_shared<IntegerType>(pivot);
}

std::shared_ptr<Type> proc_is_bool(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    bool ans = true;

    for (int i = 0; i < args_size; ++i) {
        if (args[i].get()->ToString() != "#t" && args[i].get()->ToString() != "#f") {
            ans = false;
        }
    }

    if (ans) {
        return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
    }

    return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
}

std::shared_ptr<Type> proc_not(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    if (args_size != 1) {
        throw LispRuntimeException("Unexpected amount of arguments passed to \"not\" function.");
    }

    if (args[0].get()->ToString() == "#f") {
        return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
    }

    return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
}

std::shared_ptr<Type> proc_and(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    for (int i = 0; i < args_size; ++i) {
        if (args[i].get()->ToString() == "#f") {
            return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
        }
    }
    return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
}

std::shared_ptr<Type> proc_or(std::vector<std::shared_ptr<Type>>& args) {

    std::shared_ptr<Type> ptr = nullptr;

    int args_size = args.size();
    for (int i = 0; i < args_size; ++i) {
        if(args[i].get()->ToString() != "#f") {
            ptr = args[i];
        }
    }

    if (!ptr) {
        return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
    } else {
        return ptr;
    }
}

std::shared_ptr<Type> proc_abs(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    if (args_size == 0 || args_size > 1) {
        throw LispRuntimeException("Unexpected amount of arguments passed to \"abs\" function.");
    }

    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"abs\" function.");
        }
    }

    int64_t pivot = abs(std::stoi(args[0].get()->ToString()));

    return std::make_shared<IntegerType>(pivot);
}

std::shared_ptr<Type> proc_is_pair(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    for (int i = 0 ; i < args_size; ++i) {
        if (!std::dynamic_pointer_cast<PairType>(args[i])
            || !std::dynamic_pointer_cast<PairType>(args[i]).get()->GetCur()) {
            return std::make_shared<FalseType>();
        }
    }
    return std::make_shared<TrueType>();
}

std::shared_ptr<Type> proc_is_nil(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    for (int i = 0 ; i < args_size; ++i) {
        if (std::dynamic_pointer_cast<PairType>(args[i])
            && std::dynamic_pointer_cast<PairType>(args[i]).get()->GetCur()) {
            return std::make_shared<FalseType>();
        }
    }
    return std::make_shared<TrueType>();
}

std::shared_ptr<Type> proc_cons(std::vector<std::shared_ptr<Type>>& args) {

    if (args.size() != 2) {
        throw LispRuntimeException("Unexpected amount of arguments passed to \"cons\" function.");
    }
    return std::static_pointer_cast<Type>(std::make_shared<PairType>(args[0], args[1]));
}

std::shared_ptr<Type> proc_car(std::vector<std::shared_ptr<Type>>& args) {
    auto ptr = std::dynamic_pointer_cast<PairType>(args[0]);
    return ptr.get()->GetCur();
}

std::shared_ptr<Type> proc_cdr(std::vector<std::shared_ptr<Type>>& args) {
    auto ptr = std::dynamic_pointer_cast<PairType>(args[0]);
    return ptr.get()->GetNext();
}

std::shared_ptr<Type> proc_list(std::vector<std::shared_ptr<Type>>& args) {

    int args_size = args.size();
    for (int i = 0; i < args_size; ++i) {
        if (!isIntegerType(args[i])) {
            throw LispRuntimeException("Not integer argument passed to \"list?\" function.");
        }
    }

    auto CurrentPair = std::make_shared<PairType>();
    auto cur_pair = CurrentPair;
    for (int i = 0; i < args_size; ++i) {
        auto next = std::static_pointer_cast<Type>(std::make_shared<PairType>());
        cur_pair.get()->SetCur(args[i]);
        cur_pair.get()->SetNext(next);
        cur_pair = std::dynamic_pointer_cast<PairType>(cur_pair.get()->GetNext());
    }
    return std::static_pointer_cast<Type>(CurrentPair);
}
