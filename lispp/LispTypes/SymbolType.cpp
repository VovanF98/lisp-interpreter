#include "SymbolType.h"
#include <iostream>
#include "../LispExceptions/LispNameException.h"

std::string SymbolType::ToString() {
    return *(name.get());
}

std::shared_ptr<Type> SymbolType::eval(std::shared_ptr<Scope>& scope) {

    auto searched_name = *name.get();
    if (!scope.get()->Get(name)) {
        const std::string exception_msg("There is no variable/function "
                                        + searched_name + " declared.");
        throw LispNameException(exception_msg);
    }

    return scope.get()->Get(name);
}
