#ifndef LISPP_TRUETYPE_H
#define LISPP_TRUETYPE_H
#include "Type.h"


class TrueType: public Type, public std::enable_shared_from_this<Type> {
public:
    TrueType();
    std::shared_ptr<Type> eval(std::shared_ptr<Scope>& scope) override;
    std::string ToString() override;
private:
    std::string name;
};


#endif //LISPP_TRUETYPE_H
