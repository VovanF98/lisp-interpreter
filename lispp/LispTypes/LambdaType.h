#pragma once
#include "Type.h"

class LambdaType: public Type {
public:
    LambdaType(std::shared_ptr<Scope> scope, std::vector<std::shared_ptr<Type>>& args);
    std::shared_ptr<Type> eval(std::shared_ptr<Scope>& scope) override;
    std::string ToString() override;
    std::shared_ptr<Type> callLambda(std::vector<std::shared_ptr<Type>>& args);
private:
    std::shared_ptr<Scope> scope;
    std::vector<std::shared_ptr<Type>> bodies;
    std::vector<std::shared_ptr<Type>> arguments;
};

