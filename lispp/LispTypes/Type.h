#pragma once
#ifndef LISPP_TYPE_H
#define LISPP_TYPE_H
#include <string>
#include <memory>
#include <vector>
#include <map>

class Scope;

class Type {
public:
    virtual std::string ToString();
    virtual std::shared_ptr<Type> eval(std::shared_ptr<Scope>& scope);
    virtual ~Type() = default;
};

#endif //LISPP_TYPE_H
