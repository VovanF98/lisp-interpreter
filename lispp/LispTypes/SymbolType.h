#ifndef LISPP_SYMBOLTYPE_H
#define LISPP_SYMBOLTYPE_H
#include <string>
#include <vector>
#include "Type.h"
#include "../Scope.h"


class SymbolType : public Type, public std::enable_shared_from_this<Type>  {
public:
    SymbolType(std::string& val): name(std::make_shared<std::string>(val)){};
    std::shared_ptr<Type> eval(std::shared_ptr<Scope>& scope) override;
    std::string ToString() override;

private:
    std::shared_ptr<std::string> name;
};


#endif //LISPP_SYMBOLTYPE_H
