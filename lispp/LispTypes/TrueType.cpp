#include "TrueType.h"

TrueType::TrueType() {
    name = "#t";
}

std::string TrueType::ToString() {
    return name;
}

std::shared_ptr<Type> TrueType::eval(std::shared_ptr<Scope>& scope) {
    return shared_from_this();
}