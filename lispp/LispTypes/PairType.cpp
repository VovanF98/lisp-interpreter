#include "PairType.h"
#include "../LispHelpFuncions/HelpFunctions.h"
#include <iostream>
#include <algorithm>
#include "LambdaType.h"
#include "../LispExceptions/LispRuntimeException.h"
#include "../LispExceptions/LispNameException.h"

std::string PairType::ToString() {

    if(!pairValue)
        return "()";

    std::string stringRepresentation = "(" + pairValue.get()->ToString() + " ";

    if (isIntegerType(pairNext)) {
        stringRepresentation += ". " + pairNext.get()->ToString();
    } else {

        auto currentPair = std::dynamic_pointer_cast<PairType>(pairNext);

        while (currentPair.get()->pairValue) {

            stringRepresentation += currentPair.get()->pairValue.get()->ToString() + " ";

            if (isPairType(currentPair.get()->pairNext)) {
                currentPair = std::dynamic_pointer_cast<PairType>(currentPair.get()->pairNext);
            } else {
                stringRepresentation += ". " + currentPair.get()->pairNext.get()->ToString();
                break;
            }
        }
    }

    if (stringRepresentation.back() == ' ') {
        stringRepresentation.pop_back();
    }

    return stringRepresentation + ")";
}

void PairType::SetCur(std::shared_ptr<Type>& ptr) {
    pairValue = std::move(ptr);
}

void PairType::SetNext(std::shared_ptr<Type>& ptr) {
    pairNext = std::move(ptr);
}

std::shared_ptr<Type> PairType::eval(std::shared_ptr<Scope>& scope) {

    if (!pairValue)
        throw LispRuntimeException("Runtime error");

    if (pairValue.get()->ToString() == "quote") {
        return std::dynamic_pointer_cast<PairType>(pairNext).get()->pairValue;
    }

    if (pairValue.get()->ToString() == "Quote") {
        return pairNext;
    }

    if (pairValue.get()->ToString() == "set!") {

        auto args_ = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        if (args_.empty()) {
            throw LispRuntimeException("Too few arguments passed to set!.");
        }

        auto str = args_[0]->ToString();
        if (!scope.get()->Get(std::make_shared<std::string>(str))) {
            const std::string exception_msg("There is no variable" + str + "declared yet.");
            throw LispNameException(exception_msg);
        }
        scope.get()->SetValue(str, args_[1].get()->eval(scope));

        return pairNext;
    }

    if (pairValue.get()->ToString() == "list?") {

        auto args = toVectorEval(std::dynamic_pointer_cast<PairType>(pairNext), scope);
        auto pot_list = args[0].get()->ToString();

        return isList(pot_list);
    }

    if (pairValue.get()->ToString() == "symbol?") {
        auto args = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        if(args.size() > 1)
            throw std::runtime_error("Too few arguments passed to symbol?.");

        auto argument_pair = args[0];
        auto argument = argument_pair.get()->eval(scope);
        bool answer = (isSymbolType(argument));

        if (answer) {
            return std::make_shared<TrueType>();
        } else {
            return std::make_shared<FalseType>();
        }
    }

    if (pairValue.get()->ToString() == "define") {

        auto args_ = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        auto args_size = args_.size();
        if (args_size != 2) {
            throw LispRuntimeException("Too few arguments passed to define.");
        }

        auto first_arg_pair = std::dynamic_pointer_cast<PairType>(args_[0]);
        auto first_arg_types = toVectorNoEval(first_arg_pair);
        auto first_args_size = first_arg_types.size();
        if (first_args_size >= 1) {
            auto str = first_arg_types[0].get()->ToString();
            auto first_arg_args = first_arg_pair.get()->pairNext;
            std::vector<std::shared_ptr<Type>> lambda_vector;
            lambda_vector.push_back(first_arg_args);
            for (int i = 1; i < args_size; ++i) {
                lambda_vector.push_back(args_[i]);
            }
            auto lambda_body = std::static_pointer_cast<Type>(std::make_shared<LambdaType>(scope, lambda_vector));
            scope.get()->SetValue(str, lambda_body);
        } else {
            auto str = args_[0]->ToString();
            scope.get()->SetValue(str, args_[1].get()->eval(scope));
        }

        return pairNext;
    }

    if (pairValue.get()->ToString() == "set-car!") {

        auto args_ = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        if (args_.size() != 2) {
            throw LispRuntimeException("Too few arguments passed to set-car!.");
        }

        auto str = args_[0]->ToString();
        auto node = std::dynamic_pointer_cast<PairType>(scope.get()->Get(std::make_shared<std::string>(str)));
        auto first = args_[1].get()->eval(scope);

        node.get()->SetCur(first);
        scope.get()->SetValue(str, node);

        return pairNext;
    }

    if (pairValue.get()->ToString() == "set-cdr!") {

        auto args_ = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        if (args_.size() != 2) {
            throw LispRuntimeException("Too few arguments passed to set-cdr!");
        }

        auto str = args_[0]->ToString();
        auto node = std::dynamic_pointer_cast<PairType>(scope.get()->Get(std::make_shared<std::string>(str)));
        auto first = args_[1].get()->eval(scope);
        node.get()->SetNext(first);
        scope.get()->SetValue(str, node);
        return pairNext;
    }

    if (pairValue.get()->ToString() == "and") {
        auto args_ = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        if (args_.empty()) {
            return std::static_pointer_cast<Type>(std::make_shared<TrueType>());
        }
        for(int i = 0; i < static_cast<int64_t>(args_.size()); ++i) {
            auto a = args_[i]->eval(scope).get()->ToString();
            if (a == "#f") {
                return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
            }
        }
        return args_[args_.size() - 1]->eval(scope);
    }

    if (pairValue.get()->ToString() == "or") {
        auto args_ = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        if (args_.empty()) {
            return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
        }
        for(int i = 0; i < static_cast<int64_t>(args_.size()); ++i) {
            auto a = args_[i]->eval(scope).get()->ToString();
            if (a != "#f") {
                return args_[i]->eval(scope);
            }
        }
        return std::static_pointer_cast<Type>(std::make_shared<FalseType>());
    }

    if (pairValue.get()->ToString() == "list-ref") {
        auto args = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        auto list_ref_args = std::dynamic_pointer_cast<PairType>(args[0]);
        auto nxt = std::dynamic_pointer_cast<PairType>(list_ref_args.get()->GetNext());
        auto args_ = toVectorNoEval(nxt);
        auto index = std::dynamic_pointer_cast<IntegerType>(args[1]).get()->GetValue();
        if (index >= args_.size()) {
            throw LispRuntimeException("Index in list-ref out of range.");
        }
        return args_[index];
    }

    if (pairValue.get()->ToString() == "list-tail") {
        auto args = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        auto index = std::dynamic_pointer_cast<IntegerType>(args[1]).get()->GetValue();
        auto list_begin = std::dynamic_pointer_cast<PairType>(args[0]).get()->pairNext;
        args = toVectorNoEval(std::dynamic_pointer_cast<PairType>(list_begin));
        auto str = list_begin.get()->ToString();
        if (args.size() < index)
            throw LispRuntimeException("Index in list-tail out of range.");
        while (index--) {
            list_begin = std::dynamic_pointer_cast<PairType>(list_begin).get()->pairNext;
        }
        return list_begin;
    }

    if (pairValue.get()->ToString() == "if") {
        auto args = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        if(args.size() < 2 || args.size() > 3)
            throw LispRuntimeException("Unexpectedamount of arguments passed to \"if\" construction");

        if (args[0].get()->eval(scope).get()->ToString() != "#f")
            return args[1].get()->eval(scope);
        else {
            if(args.size() == 3) {
                return args[2].get()->eval(scope);
            }
            return std::make_shared<PairType>();
        }
    }

    if (pairValue.get()->ToString() == "lambda") {
        auto args_and_bodies = toVectorNoEval(std::dynamic_pointer_cast<PairType>(pairNext));
        if (args_and_bodies.size() < 2) {
            throw LispRuntimeException("Unexpected amount of bodies passed to lambda.");
        }
        return std::static_pointer_cast<Type>(std::make_shared<LambdaType>(scope, args_and_bodies));
    }


    auto func = pairValue.get()->eval(scope);

    auto a = pairValue.get()->ToString();

    if (isLambdaType(func)) {
        auto args_lambda_eval = toVectorEval(std::dynamic_pointer_cast<PairType>(pairNext), scope);
        return std::dynamic_pointer_cast<LambdaType>(func).get()->callLambda(args_lambda_eval);
    } else {
        auto args = toVectorEval(std::dynamic_pointer_cast<PairType>(pairNext), scope);
        auto extracted = scope.get()->Get(std::make_shared<std::string>(a));
        if(!extracted) {
            const std::string exception_msg = "No variable " + a + " declared.";
            throw LispNameException(exception_msg);
        }
        if (isFunctionType(extracted)) {
            return std::dynamic_pointer_cast<Function>(extracted).get()->func_(args);
        }
        else
            return extracted;
    }
}

PairType::PairType(std::shared_ptr<Type> cur_tp, std::shared_ptr<Type> nxt_nd) {
    pairValue = cur_tp;
    pairNext = nxt_nd;
}

std::shared_ptr<Type> PairType::GetCur() {
    return pairValue;
}

std::shared_ptr<Type> PairType::GetNext() {
    return pairNext;
}
