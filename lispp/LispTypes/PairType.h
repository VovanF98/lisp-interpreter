#pragma once

#include "Type.h"

class PairType: public Type {
public:
    PairType(std::shared_ptr<Type> cur_tp, std::shared_ptr<Type> nxt_nd);

    PairType(){
        pairNext = nullptr;
        pairValue = nullptr;
    }

    std::shared_ptr<Type> eval(std::shared_ptr<Scope>& scope) override;

    std::string ToString() override;

    std::shared_ptr<Type> GetNext();

    std::shared_ptr<Type> GetCur();

    void SetCur(std::shared_ptr<Type>& ptr);

    void SetNext(std::shared_ptr<Type>& ptr);
private:
    std::shared_ptr<Type> pairNext;
    std::shared_ptr<Type> pairValue;
};

