#ifndef LISPP_INTEGERTYPE_H
#define LISPP_INTEGERTYPE_H
#include <stdint-gcc.h>
#include <string>
#include "Type.h"
#include <vector>

class IntegerType : public Type, public std::enable_shared_from_this<Type>  {
public:
    IntegerType(int64_t& val): value(std::make_shared<int64_t>(val)) {};
    std::string ToString() override;
    std::shared_ptr<Type> eval(std::shared_ptr<Scope>& scope) override;
    int64_t GetValue();
private:
    std::shared_ptr<int64_t> value;
    std::vector<std::shared_ptr<Type>> args;
};


#endif //LISPP_INTEGERTYPE_H
