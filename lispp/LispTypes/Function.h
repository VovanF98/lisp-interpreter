#ifndef LISPP_FUNCTION_H
#define LISPP_FUNCTION_H

#include "Type.h"
#include "../Scope.h"


class Function: public Type, public std::enable_shared_from_this<Type> {
public:

    Function(std::shared_ptr<Type> (*func)(std::vector<std::shared_ptr<Type>>&));

    std::shared_ptr<Type> eval(std::shared_ptr<Scope>& scope) override;

    std::string ToString() override;

    std::shared_ptr<Type> (*func_)(std::vector<std::shared_ptr<Type>>&);

private:

    std::shared_ptr<Scope> scope;
};


#endif //LISPP_FUNCTION_H
