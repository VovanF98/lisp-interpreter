#include "LambdaType.h"
#include "../LispHelpFuncions/HelpFunctions.h"
#include "../LispExceptions/LispRuntimeException.h"

std::shared_ptr<Type> LambdaType::eval(std::shared_ptr<Scope> &scope) {
    return std::make_shared<Type>();
}

std::string LambdaType::ToString() {
    return "<Lambda>";
}

LambdaType::LambdaType(std::shared_ptr<Scope> scp,
                       std::vector<std::shared_ptr<Type>>& args): scope(std::make_shared<Scope>()) {

    scope = std::make_shared<Scope>(scp);
    arguments = toVectorNoEval(std::dynamic_pointer_cast<PairType>(args[0]));
    auto args_size = args.size();

    for (int i = 1; i < args_size; ++i) {
        bodies.push_back(args[i]);
    }
}

std::shared_ptr<Type> LambdaType::callLambda(std::vector<std::shared_ptr<Type>>& args) {

    if (args.size() < arguments.size())
        throw LispRuntimeException("There are too few arguments/bodies in lambda");

    for (int i = 0; i < arguments.size(); ++i) {
        scope.get()->SetValue(arguments[i].get()->ToString(), args[i]);
    }

    std::shared_ptr<Type> last_body_eval;

    auto bodies_size = bodies.size();
    for (int i = 0; i < bodies_size; ++i) {
        last_body_eval = bodies[i].get()->eval(scope);
    }

    return last_body_eval;
}
