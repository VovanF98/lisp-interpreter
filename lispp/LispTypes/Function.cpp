#include "Function.h"

std::string Function::ToString() {
    return std::string("<Function>");
}

std::shared_ptr<Type> Function::eval(std::shared_ptr<Scope>& scp) {
    return std::make_shared<Type>();
}

Function::Function(std::shared_ptr<Type> (*func)(std::vector<std::shared_ptr<Type>> &) ) {
    func_ = func;
}