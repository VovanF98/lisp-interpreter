#include "FalseType.h"


std::shared_ptr<Type> FalseType::eval(std::shared_ptr<Scope>& scope) {
    return shared_from_this();
}

std::string FalseType::ToString() {
    return name;
}

FalseType::FalseType() {
    name = "#f";
}