#ifndef LISPP_FALSETYPE_H
#define LISPP_FALSETYPE_H

#include "Type.h"
#include <memory>


class FalseType: public Type, public std::enable_shared_from_this<Type> {
public:
    FalseType();
    std::shared_ptr<Type> eval(std::shared_ptr<Scope>& scope) override;
    std::string ToString() override;
private:
    std::string name;
};


#endif //LISPP_FALSETYPE_H
