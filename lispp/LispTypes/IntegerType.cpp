#include "IntegerType.h"

std::shared_ptr<Type> IntegerType::eval(std::shared_ptr<Scope>& scope) {
    return shared_from_this();
}

std::string IntegerType::ToString() {
   return std::to_string(*(value.get()));
}

int64_t IntegerType::GetValue() {
    return *value.get();
}