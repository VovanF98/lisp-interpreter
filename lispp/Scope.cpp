#include "Scope.h"
#include "AllTypes.h"
#include "LispBuiltInFunctions/LispStandartFunctions.h"

Scope::Scope() {
    outer_scope = nullptr;
    cur_scope = std::make_shared<std::map<std::string, std::shared_ptr<Type>>>();
    AddGlobals();
}

Scope::Scope(std::shared_ptr<Scope> scp): outer_scope(scp),
                                          cur_scope(std::make_shared<std::map<std::string,
                                                  std::shared_ptr<Type>>>()) {};

void Scope::clearScope() {
    for(auto &i : *cur_scope.get()) {
        i.second.reset();
    }
    cur_scope.get()->clear();
}

std::shared_ptr<std::map<std::string, std::shared_ptr<Type>>> Scope::GetScope() {
    return cur_scope;
}

std::shared_ptr<Type> Scope::Get(std::shared_ptr<std::string> str) {
    if(isInScope(*str.get()))
        return (*cur_scope.get())[*str.get()];
    else
        if (outer_scope)
            return outer_scope.get()->Get(str);
        else
            return nullptr;
}

bool Scope::isInScope(std::string &str) {
    return (cur_scope.get()->find(str) != cur_scope.get()->end());
}

void Scope::SetValue(std::string key, std::shared_ptr<Type> value) {
    (*cur_scope.get())[key] = value;
}

void Scope::AddGlobals() {
    auto env = cur_scope.get();
    (*env)["#f"] = std::make_shared<FalseType>();
    (*env)["#t"] = std::make_shared<TrueType>();
    (*env)["car"] = std::make_shared<Function>(&proc_car);
    (*env)["cdr"] = std::make_shared<Function>(&proc_cdr);
    (*env)["cons"] = std::make_shared<Function>(&proc_cons);
    (*env)["list"] = std::make_shared<Function>(&proc_list);
    (*env)["boolean?"] = std::make_shared<Function>(&proc_is_bool);
    (*env)["null?"] = std::make_shared<Function>(&proc_is_nil);
    (*env)["pair?"] = std::make_shared<Function>(&proc_is_pair);
    (*env)["and"] = std::make_shared<Function>(&proc_and);
    (*env)["abs"] = std::make_shared<Function>(&proc_abs);
    (*env)["or"] = std::make_shared<Function>(&proc_or);
    (*env)["max"] = std::make_shared<Function>(&proc_max);
    (*env)["not"] = std::make_shared<Function>(&proc_not);
    (*env)["min"] = std::make_shared<Function>(&proc_min);
    (*env)["number?"] = std::make_shared<Function>(&proc_is_int);
    (*env)["+"] = std::make_shared<Function>(&proc_add);
    (*env)["="] = std::make_shared<Function>(&proc_equal);
    (*env)["-"] = std::make_shared<Function>(&proc_sub);
    (*env)["*"] = std::make_shared<Function>(&proc_mul);
    (*env)["/"] = std::make_shared<Function>(&proc_div);
    (*env)[">"] = std::make_shared<Function>(&proc_greater);
    (*env)["<"] = std::make_shared<Function>(&proc_less);
    (*env)["<="] = std::make_shared<Function>(&proc_less_equal);
    (*env)[">="] = std::make_shared<Function>(&proc_greater_equal);
}