#pragma once
#include <map>
#include <memory>
#include "LispTypes/Type.h"

class Scope {
public:
    Scope(std::shared_ptr<Scope> scp);
    Scope();
    std::shared_ptr<std::map<std::string, std::shared_ptr<Type>>> GetScope();
    std::shared_ptr<Type> Get(std::shared_ptr<std::string> str);
    void AddGlobals();
    void SetValue(std::string key, std::shared_ptr<Type> value);
    bool isInScope(std::string& str);
    void clearScope();
private:
    std::shared_ptr<Scope> outer_scope;
    std::shared_ptr<std::map<std::string, std::shared_ptr<Type>>> cur_scope;
};
