#include "lisp_test.h"

TEST_CASE_METHOD(LispTest, "BooleansAreSelfEvaluating") {
    ExpectEq("#t", "#t");
    ExpectEq("#f", "#f");
}

TEST_CASE_METHOD(LispTest, "BooleanPredicate") {
    ExpectEq("(boolean? #t)", "#t");
    ExpectEq("(boolean? #f)", "#t");
    ExpectEq("(boolean? 1)", "#f");
    ExpectEq("(boolean? '())", "#f");
}

TEST_CASE_METHOD(LispTest, "NotFunction") {
    ExpectEq("(not #f)", "#t");
    ExpectEq("(not #t)", "#f");
    ExpectEq("(not 1)", "#f");
    ExpectEq("(not 0)", "#f");
    ExpectEq("(not '())", "#f");
}

TEST_CASE_METHOD(LispTest, "NotFunctionInvalidCall") {
    ExpectRuntimeError("(not)");
    ExpectRuntimeError("(not #t #t)");
}

TEST_CASE_METHOD(LispTest, "AndSyntax") {
    ExpectEq("(and)", "#t");
    ExpectEq("(and (= 2 2) (> 2 1))", "#t");
    ExpectEq("(and (= 2 2) (< 2 1))", "#f");
    ExpectEq("(and 1 2 'c '(f g))", "(f g)");
}

TEST_CASE_METHOD(LispTest, "AndOptimizesArgumentEvaluation") {
    ExpectNoError("(define x 1)");
    ExpectNoError("(and #f (set! x 2))");
    ExpectEq("x", "1");
}

TEST_CASE_METHOD(LispTest, "OrSyntax") {

    ExpectEq("(or)", "#f");
    ExpectEq("(or (not (= 2 2)) (> 2 1))", "#t");
    ExpectEq("(or #f (< 2 1))", "#f");
    ExpectEq("(or #f 1)", "1");
}

TEST_CASE_METHOD(LispTest, "OrOptimizesArgumentEvaluation") {
    ExpectNoError("(define x 1)");
    ExpectNoError("(or #t (set! x 2))");
    ExpectEq("x", "1");
}
