#pragma once
#include <catch.hpp>
#include <iostream>
#include "../lispp/LispInterpreter/Interpreter.h"

struct LispTest {
    std::shared_ptr<Interpreter> lisp;

    LispTest() {
        lisp = std::make_shared<Interpreter>();
    }

    void ExpectEq(std::string expression, std::string result) {
        auto answer = lisp.get()->run(expression);
        REQUIRE(answer == result);
    }

    void ExpectNoError(std::string expression) {
        int error;
        try {
            lisp.get()->run(expression);
            error = 0;
        }
        catch (const std::logic_error& ex) {
            error = 1;
        }
        REQUIRE(error == 0);
    }

    void ExpectSyntaxError(std::string expression) {
        int error;
        try {
            lisp.get()->run(expression);
            error = 0;
        }
        catch (const std::logic_error& ex) {
            error = 1;
        }
        REQUIRE(error == 1);
    }

    void ExpectRuntimeError(std::string expression) {
        int error;
        try {
            lisp.get()->run(expression);
            error = 0;
        }
        catch (const std::logic_error& ex) {
            error = 1;
        }
        REQUIRE(error == 1);
    }

    void ExpectNameError(std::string expression) {
        int error;
        try {
            lisp.get()->run(expression);
            error = 0;
        }
        catch (const std::logic_error& ex) {
            error = 1;
        }
        REQUIRE(error == 1);
    }
};
