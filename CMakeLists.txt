cmake_minimum_required(VERSION 3.5)
project(lispp)

include(../common.cmake)

add_library(lispp-lib
        lispp/LispTypes/Type.cpp lispp/LispTypes/Type.h
        lispp/LispTypes/SymbolType.cpp lispp/LispTypes/SymbolType.h
        lispp/LispTypes/IntegerType.cpp lispp/LispTypes/IntegerType.h
        lispp/LispTypes/PairType.cpp lispp/LispTypes/PairType.h
        lispp/LispTypes/Function.cpp lispp/LispTypes/Function.h
        lispp/LispTypes/LambdaType.cpp lispp/LispTypes/LambdaType.h
        lispp/LispHelpFuncions/HelpFunctions.cpp lispp/LispHelpFuncions/HelpFunctions.h
        lispp/LispTypes/FalseType.cpp lispp/LispTypes/FalseType.h
        lispp/LispTypes/TrueType.cpp lispp/LispTypes/TrueType.h
        lispp/LispInterpreter/Parser.cpp lispp/LispInterpreter/Parser.h
        lispp/LispInterpreter/Interpreter.cpp lispp/LispInterpreter/Interpreter.h
        lispp/LispBuiltInFunctions/LispStandartFunctions.cpp lispp/LispBuiltInFunctions/LispStandartFunctions.h
        lispp/LispBuiltInFunctions/LispSpecialFunctions.cpp lispp/LispBuiltInFunctions/LispSpecialFunctions.h
        lispp/LispExceptions/LispNameException.cpp lispp/LispExceptions/LispNameException.h
        lispp/LispExceptions/LispRuntimeException.cpp lispp/LispExceptions/LispRuntimeException.h
        lispp/LispExceptions/LispSyntaxException.cpp lispp/LispExceptions/LispSyntaxException.h
        test/test_boolean.cpp test/test_control_flow.cpp test/test_eval.cpp test/test_integer.cpp
        test/test_lambda.cpp test/test_list.cpp test/test_symbol.cpp test/lisp_test.h
        lispp/AllTypes.h
        lispp/Scope.cpp lispp/Scope.h
        lispp/catch_Main.cpp)

add_executable(lispp
        test/test_integer.cpp
        test/test_eval.cpp
        test/test_boolean.cpp
        test/test_list.cpp
        test/test_symbol.cpp
        test/test_control_flow.cpp
        test/test_lambda.cpp)

target_link_libraries(lispp
        lispp-lib)


add_executable(test_lispp
        test/test_integer.cpp
        test/test_eval.cpp
        test/test_boolean.cpp
        test/test_list.cpp
        test/test_symbol.cpp
        test/test_control_flow.cpp
        test/test_lambda.cpp)

target_link_libraries(test_lispp
        lispp-lib)
